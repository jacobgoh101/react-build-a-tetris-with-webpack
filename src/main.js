global.jQuery = require('jquery');
const $ = global.jQuery;
const React = require('react');
const ReactDOM = require('react-dom');
const rn = require('random-number');
import {store} from './modules/reducer.js';
const keyPress = require('./modules/keyPress.js');
keyPress();
import {generateNewBlock, moveBlockDown, checkForCompletedRow} from './modules/functions-for-main.js';
require('./style.scss');

const App = ({
	cellsReducer
}) => {
	const cellsState = cellsReducer.allCellsState;
	const points = cellsReducer.points;
	const blocksGenerated = cellsReducer.blocksGenerated;
	const lost = cellsReducer.lost;
	const hiddenBeforeLost = lost ? '' : ' hidden';
	const hiddenAfterLost = !lost ? '' : ' hidden';
	const pause = cellsReducer.pause;
	let mapIndex = 1;
	let cellsOutput = cellsState.map(function(cellState){
		let className = "cell " + cellState + " cell-" + mapIndex;
		if( (mapIndex-1) % 10 == 0 ) {
			className += " clear-left";
		}
		return(
			<div className={className} key={"cell-" + String(mapIndex++) }></div>
		);
	});
	let togglePause = () => {
		store.dispatch({
			type: 'TOGGLEPAUSE'
		});
	}
	return (
		<div>
			<div className="info-container">
				<h3 className="text-primary">Points: {points}</h3>
				<h3 className="text-primary">Blocks Generated: {blocksGenerated}</h3>
				<br /><br />
				<button className={"btn btn-lg btn-default"+hiddenAfterLost} onClick={togglePause}>{pause ? 'Resume':'Pause'}</button>
				<br /><br />
				<h3 className={"text-danger " + hiddenBeforeLost }>Game Lost</h3>
			</div>
			<div className="cells-container clearfix">
				<div className="clearfix">
					{cellsOutput}
				</div>
			</div>
		</div>
	);
}

const render = () => {
	if(store.getState().cellsReducer.lost){
		alert('You have lost the game.')
	}
	ReactDOM.render(<App cellsReducer={store.getState().cellsReducer} />, document.getElementById('root'));
}

store.subscribe(render);
render();
generateNewBlock();

setInterval( () => {
	if(store.getState().cellsReducer.pause) return;
	if(store.getState().cellsReducer.lost) return;
	moveBlockDown();
},1000);
