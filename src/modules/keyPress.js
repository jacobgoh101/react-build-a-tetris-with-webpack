global.jQuery = require('jquery');
const $ = global.jQuery;
import {store} from './reducer.js';

module.exports = () => {
  $(document).keydown(function(e) {
    switch(e.which) {
      case 37: // left
      store.dispatch({
        type: 'MOVE',
        direction: 'left'
      });
      break;
      case 39: // right
      store.dispatch({
        type: 'MOVE',
        direction: 'right'
      });
      break;
      case 40: // down
      store.dispatch({
        type: 'MOVE',
        direction: 'down'
      });
      break;
      case 38: // up
      store.dispatch({
        type: 'MOVE',
        direction: 'up'
      });
      break;
      default:
      return; // exit this handler for other keys
    }
    e.preventDefault();
  });
}
