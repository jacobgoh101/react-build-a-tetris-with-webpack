const rn = require('random-number');
import {store} from './reducer.js';

const generateNewBlock = () => {
	store.dispatch({
		type: 'CREATE',
		blockID: rn({
			min:  0,
			max:  6,
			integer: true
		})
		// blockID: 3
	});
}

const moveBlockDown = () => {
	if(!!store.getState().cellsReducer.activeCells.length) {
		store.dispatch({
			type: 'MOVE',
			direction: 'down'
		});
	}
}

const checkForCompletedRow = () => {
	const allCellsState = store.getState().cellsReducer.allCellsState;
	for (let row = 20; row > 0; row--) {
		let filledCellsInThisRow = 0;
		for (let col = 1; col <= 10; col++) {
			let thisCellIndex = (row-1)*10 + col;
			if(allCellsState[thisCellIndex].indexOf('filled') > -1) filledCellsInThisRow++;
		}
		if(filledCellsInThisRow == 10) {
			return store.dispatch({
				type: 'CLEARFULLYFILLEDROW',
				row
			});
		}
	}
}

module.exports = {generateNewBlock, moveBlockDown, checkForCompletedRow}
