import * as Blocks from './blocksDeclaration.js';
const BlocksArray = Object.keys(Blocks);
const rn = require('random-number');
// import {moveBlockLeftRight, moveBlockDown, rotateBlock} from './reducer-functions-moveBlock.js';

const createBlock = (cellsReducer, blockID) => {
	if(cellsReducer.lost) return cellsReducer;
	// handle allCellsState
	let cells = cellsReducer.allCellsState;
	let blockName = BlocksArray[blockID];
	let rotateIndex = 0;
	let thisBlock = Blocks[blockName]['block'][rotateIndex];
	let thisBlockColor = Blocks[blockName]['color'];
	let i = 0;
	let cellIndexForCreate = [4,5,6,7,14,15,16,17,24,25,26,27,34,35,36,37];
	let activeCells = [];
	let lost = false;
	let newCells = cells.map((cell,index) => {
		if(lost) return cell;
		if( cellIndexForCreate.indexOf(index) > -1 ) {
			if(cell.indexOf('filled') > -1) {
			  // newly create cell will clash with filled cell. player lose
			  lost = true;
			}
			if( thisBlock[i++] ) {
				activeCells.push(index);
				return 'filled active ' + thisBlockColor;
			}else{
				return 'empty';
			}
		}
		return cell;
	});

	if(lost) return {...cellsReducer,
		lost
	};

	return { ...cellsReducer,
		allCellsState: newCells,
		activeCells,
		blockID,
		rotateIndex,
		rotateKeyCell: 15,
		blocksGenerated: cellsReducer.blocksGenerated+1
	}
};

const moveBlockLeftRight = (cellsReducer, direction) => {
	let cells = cellsReducer.allCellsState;
	let activeCells = cellsReducer.activeCells;
	let cannotMove = false;
	let activeCellClassName = null;
	cells.map((cell,index) => {
		if(cannotMove) return;
		if(cell.indexOf('active') > -1) {
			//store active cell class name
			if(!activeCellClassName) activeCellClassName = cell;

			if( index%10 == (direction == 'left' ? 1 : 0) ) {
				// active at the leftest already
				cannotMove = true;
				return cell;
			} else if (cells[(direction == 'left' ? index-1 : index+1)].indexOf('filled') > -1 && cells[(direction == 'left' ? index-1 : index+1)].indexOf('active') < 0) {
				// clash with filled cell, but not active filled cell
				cannotMove = true;
				return cell;
			}
		}
	});
	if(cannotMove) return moveBlockDown(cellsReducer, direction);
	else {
		let newActiveCells = [];

		//remove current active cell
		let newCellsState =  cells.map((cell,index) => {
			if( activeCells.indexOf(index) > -1 ) {
				return 'empty';
			}
			return cell;
		});

		//add state to the newly active left cell
		newCellsState =  newCellsState.map((cell,index) => {
			if( activeCells.indexOf( direction == 'left' ? index+1 : index-1 ) > -1 ) {
				// is at the left of active cell
				newActiveCells.push(index);
				return activeCellClassName;
			}
			return cell;
		});
		return {...cellsReducer,
			allCellsState: newCellsState,
			activeCells: newActiveCells,
			rotateKeyCell: direction == 'left' ? (cellsReducer.rotateKeyCell - 1) : (cellsReducer.rotateKeyCell + 1)
		};
	}
	return cellsReducer;
}

const clearFullyFilledRow = (cellsReducer, filledRow) => {
	const allCellsState = cellsReducer.allCellsState;
	let newAllCellsState = allCellsState.slice();

	//empty all filled row cells
	for (let col = 1; col <= 10; col++) {
		let thisCellIndex = (filledRow-1)*10 + col;
		newAllCellsState[thisCellIndex] = 'empty';
	}

	//clone from upper cell
	for (let row = filledRow; row > 0; row--) {
		for (let col = 1; col <= 10; col++) {
			let thisCellIndex = (row-1)*10 + col;
			let upperCellIndex = thisCellIndex - 10;
			newAllCellsState[thisCellIndex] = newAllCellsState[upperCellIndex];
			if(!newAllCellsState[thisCellIndex]) newAllCellsState[thisCellIndex] = 'empty';
		}
	}
	return {...cellsReducer,
		allCellsState: newAllCellsState,
		points: cellsReducer.points + 10
	}
};

const moveBlockDown= (cellsReducer, direction) => {
	let cells = cellsReducer.allCellsState;
	let activeCells = cellsReducer.activeCells;
	let cannotMove = false;
	if(!activeCells.length) cannotMove = true;
	let activeCellClassName = null;
	cells.map((cell,index) => {
		if(cannotMove) return;
		if(!cell) console.log(cells);
		if(cell.indexOf('active') > -1) {
			//store active cell class name
			if(!activeCellClassName) activeCellClassName = cell;

			if( index > 190 ) {
				// active at the bottom already
				cannotMove = true;
				return cell;
			} else if (cells[index+10].indexOf('filled') > -1 && cells[index+10].indexOf('active') < 0) {
				// clash with filled cell, but not active filled cell
				cannotMove = true;
				return cell;
			}
		}
	});
	if(cannotMove){
		// cannotMove, time to fix the block and generate new block
		let newCellsState =  cells.map((cell,index) => {
			if( activeCells.indexOf(index) > -1 ) {
				return cell.replace('active','fixed');
			}
			return cell;
		});

		// save newCellsState into a new cellsReducer and check for completed row
		let newCellsReducer = {...cellsReducer,
			allCellsState: newCellsState,
			blockID: null,
			rotateIndex: 0,
			rotateKeyCell: null,
			activeCells: []
		};
		let lastCompletedRow = getLastCompletedRow(newCellsReducer.allCellsState);
		while(lastCompletedRow) { //has completed row
			newCellsReducer = clearFullyFilledRow(newCellsReducer, lastCompletedRow);
			lastCompletedRow = lastCompletedRow = getLastCompletedRow(newCellsReducer.allCellsState);
		}
		return createBlock(newCellsReducer, rn({
			min:  0,
			max:  6,
			integer: true
		}));
	}
	else {
		let newActiveCells = [];

		//remove current active cell
		let newCellsState =  cells.map((cell,index) => {
			if( activeCells.indexOf(index) > -1 ) {
				return 'empty';
			}
			return cell;
		});

		//add state to the newly active left cell
		newCellsState =  newCellsState.map((cell,index) => {
			if( activeCells.indexOf( index-10 ) > -1 ) {
				// is at the bottom of active cell
				newActiveCells.push(index);
				return activeCellClassName;
			}
			return cell;
		});
		return {...cellsReducer,
			allCellsState: newCellsState,
			activeCells: newActiveCells,
			rotateKeyCell: cellsReducer.rotateKeyCell + 10
		};
	}
	return cellsReducer;
}
const rotateBlock = (cellsReducer, direction) => {
	const get16CellsFromRotateKeyCell = (rotateKeyCell) => {
		let cells = [];
		let n = 0;
		for (let i = 0; i < 4; i++) {
			for (let j = 0; j < 4; j++) {
				cells[n++] = rotateKeyCell - (1-i)*10 - (1-j) ;
			}
		}
		return cells;
	};
	const blockID = cellsReducer.blockID;
	const rotateIndex = cellsReducer.rotateIndex;
	const rotateKeyCell = cellsReducer.rotateKeyCell;
	const allCellsState = cellsReducer.allCellsState;
	const activeCells = cellsReducer.activeCells;
	const blockName = BlocksArray[blockID];
	let thisBlock = Blocks[blockName];
	thisBlock = thisBlock.block;
	const currentActiveCellsMap = thisBlock[rotateIndex];
	const newActiveCellsMap = thisBlock[ (rotateIndex + 1) <= 3 ? (rotateIndex + 1) : 0 ];
	const full16CellsMap = get16CellsFromRotateKeyCell(rotateKeyCell);
	let willClash = false;
	let cellsThatAreGoingToBeActive = [];

	//get cellsThatAreGoingToBeActive
	full16CellsMap.map((cellIndex, mapIndex) => {
		if(newActiveCellsMap[mapIndex]) cellsThatAreGoingToBeActive.push(cellIndex);
	});

	//see if the cell will clash with left/right wall
	let cellsThatAreGoingToBeActive_RowIndexOnly = cellsThatAreGoingToBeActive.map((cellIndex) => {
		let rowIndex = cellIndex % 10;
		if(rowIndex == 0) rowIndex = 10;
		return rowIndex;
	});
	let maxRowIndex = Math.max(...cellsThatAreGoingToBeActive_RowIndexOnly);
	let minRowIndex = Math.min(...cellsThatAreGoingToBeActive_RowIndexOnly);
	if(maxRowIndex - minRowIndex > 3) willClash = true;
	//will clash when max and min are obviously at the 2 side of the wall
	//3 is the longest distance in block itself

	//see if it will clash with filled cell
	cellsThatAreGoingToBeActive.map((cellIndex) => {
		if(willClash) return;
		if(allCellsState[cellIndex].indexOf('filled') > -1 && allCellsState[cellIndex].indexOf('active') < 0) willClash = true;
	});
	console.log(willClash ? 'will clash' : 'will not clash');
	if(willClash) return cellsReducer;
	else{
		let activeCellClassName = null;

		//remove current active cell
		let newCellsState =  allCellsState.map((cell,index) => {
			if( activeCells.indexOf(index) > -1 ) {
				//store active cell class name
				if(!activeCellClassName) activeCellClassName = cell;

				return 'empty';
			}
			return cell;
		});
		newCellsState =  newCellsState.map((cell,index) => {
			if( cellsThatAreGoingToBeActive.indexOf(index) > -1 ) {
				return activeCellClassName;
			}
			return cell;
		});
		return {...cellsReducer,
			allCellsState: newCellsState,
			rotateIndex: (rotateIndex + 1) <= 3 ? (rotateIndex + 1) : 0,
			activeCells: cellsThatAreGoingToBeActive
		}
	}

	return cellsReducer;
}

const getLastCompletedRow = (allCellsState) => {
	let lastCompletedRow = null;
	for (let row = 20; row > 0; row--) {
		let filledCellsInThisRow = 0;
		for (let col = 1; col <= 10; col++) {
			let thisCellIndex = (row-1)*10 + col;
			if(allCellsState[thisCellIndex].indexOf('filled') > -1) filledCellsInThisRow++;
		}
		if(filledCellsInThisRow == 10) {
			lastCompletedRow = row;
			break;
		}
	}
	return lastCompletedRow;
}

const moveBlock = (cellsReducer, direction) => {
	if(cellsReducer.lost) return cellsReducer;
	if(cellsReducer.pause) return cellsReducer;
	switch(direction) {
		case 'left':
		case 'right':
		return moveBlockLeftRight(cellsReducer, direction);
		break;
		case 'down':
		return moveBlockDown(cellsReducer, direction);
		break;
		case 'up':
		return rotateBlock(cellsReducer, direction);
		break;
		default:
		return cellsReducer;
	}
};

module.exports = {createBlock, moveBlock};
