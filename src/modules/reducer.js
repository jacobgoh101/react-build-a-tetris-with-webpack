import {Redux, createStore, combineReducers, applyMiddleware} from 'redux';
import * as Blocks from './blocksDeclaration.js';
import {logger} from './reducer-logger.js';
import {createBlock, moveBlock} from './reducer-functions.js';
const BlocksArray = Object.keys(Blocks);

const cellsInitObj = [];
for (let i = 1; i <= 200; i++) {
  cellsInitObj[i] = 'empty';
}
const cellsReducer = (state = {
  blockID: null,
  rotateIndex: 0,
  rotateKeyCell: null, // the 2nd cell in 2nd row in a block
  activeCells: [],
  allCellsState: cellsInitObj,
  points: 0,
  blocksGenerated: 0,
  lost: false,
  pause: false
}, action) => {
  switch (action.type) {
    case 'CREATE':
    return createBlock(state, action.blockID);
    break;
    case 'MOVE':
    return moveBlock(state, action.direction);
    break;
    case 'TOGGLEPAUSE':
    return {...state,
      pause: !state.pause,
    };
    break;
    default:
    return state;
  }
}

const mainReducer = combineReducers({
  cellsReducer
});

const store = createStore(mainReducer,
  applyMiddleware(
    logger
  )
);

module.exports = {store};
