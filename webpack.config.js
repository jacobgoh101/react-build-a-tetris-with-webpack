const webpack = require('webpack');
const AggressiveMergingPlugin = require("./node_modules/webpack/lib/optimize/AggressiveMergingPlugin.js");
const DedupePlugin = require("./node_modules/webpack/lib/optimize/DedupePlugin.js");
const UglifyJsPlugin = require("./node_modules/webpack/lib/optimize/UglifyJsPlugin.js");

module.exports = {
	entry: './src/main.js',
	output: {
		path: 'build',
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				loader: 'babel',
				query: {
					presets: ['react', 'es2015']
				}
			},
			{
				test: /\.scss$/,
				loader: 'style-loader!css-loader!sass-loader'
			},
			{
				test: /\.css$/,
				loader: 'style-loader!css-loader'
			},
			{
				test: /\.(png|jpg)$/,
				loader: 'url'
			}
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			// <-- key to reducing React's size
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		}),
		new webpack.optimize.DedupePlugin(), //dedupe similar code
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false
			}
		}), //minify everything
		new webpack.optimize.AggressiveMergingPlugin()//Merge chunks
	],
};
